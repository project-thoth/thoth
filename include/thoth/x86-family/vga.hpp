/*
* 	filename  : vga.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_x86_FAMILY_VGA_HPP
#define _THOTH_x86_FAMILY_VGA_HPP

// Ibis headers
#include <ibis/value.hpp>
#include <ibis/stdio.hpp>

// Thoth headers
#include <thoth/vfs/vfilesession.hpp>

// Local headers
#include "vgadef.hpp"

namespace thoth
{
	extern uint16 VGA_WIDTH;
	extern uint16 VGA_HEIGHT;

	extern bool vga_initiated;

	enum class vga_inherit_mode { NONE, CURSOR, ALL, };

	ibis::status vga_init(vga_inherit_mode _inherit_mode = vga_inherit_mode::NONE);
	uint8        vga_make_colour(vga_colour _fg, vga_colour _bg);
	uint16       vga_make_entry(char _c, uint8 _colour);
	void         vga_place_entry(char _c, uint8 _colour, memint _i, memint _j);
	void         vga_place_cursor(memint _i, memint _j);

	void vga_clear(uint8 colour);
	void vga_print_char(char _c);
	void vga_print_string(const char* _str);
	void vga_process_msg(const ibis::file_msg _msg, const vfilesession* _session);
};

// End header guard
#endif
