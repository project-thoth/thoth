/*
* 	filename  : serial.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_x86_FAMILY_SERIAL_HPP
#define _THOTH_x86_FAMILY_SERIAL_HPP

// Ibis headers
#include <ibis/stdio.hpp>
#include <thoth/vfs/vfs.hpp>

namespace thoth
{
	ibis::status serial_init(ibis::string _node = "/dev/serial");
	void         serial_process_msg(const ibis::file_msg _msg, const vfilesession* _session);
	void         serial_port_process_msg(const ibis::file_msg _msg, const vfilesession* _session);
	void         serial_init_port(unsigned short _port, int _baudrate, byte _databits, byte _stopbits, byte _parity);
};

// End header guard
#endif
