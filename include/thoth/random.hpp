/*
* 	filename  : random.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_RANDOM_HPP
#define _THOTH_RANDOM_HPP

// Ibis headers
#include <ibis/stdio.hpp>

// Thoth headers
#include <thoth/vfs/vfs.hpp>

namespace thoth
{
	ibis::status random_init(ibis::string _node = "/dev/random");
	void         random_process_msg(const ibis::file_msg _msg, const vfilesession* _session);
};

// End header guard
#endif
