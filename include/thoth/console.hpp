/*
* 	filename  : console.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_CONSOLE_HPP
#define _THOTH_CONSOLE_HPP

// Thoth headers
#include <thoth/tty.hpp>
#include <thoth/vfs/vfilesession.hpp>

// Ibis headers
#include <ibis/stdio.hpp>

namespace thoth
{
	extern ibis::file console_stdin;
	extern ibis::file console_stdout;

	ibis::status               console_init(ibis::string _in, ibis::string _out);
	ibis::value<unsigned long> console_gen_tty();
	ibis::value<ibis::string>  console_get_tty(unsigned long _i);
	ibis::status               console_switch_tty(unsigned long _i);
	void                       console_process_msg(const ibis::file_msg _msg, const vfilesession* _session);
	void                       console_process_tty_msg(const ibis::file_msg __msg, const vfilesession* _session);
};

// End header guard
#endif
