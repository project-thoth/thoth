/*
* 	filename  : vfilesession.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_VFS_VFILESESSION_HPP
#define _THOTH_VFS_VFILESESSION_HPP

// Ibis headers
#include <ibis/stdio.hpp>

// Thoth headers
#include <thoth/vfs/vfs.hpp>

namespace thoth
{
	struct vfilesession
	{
	private:

		ibis::file _id;
		ibis::string _vnode;

	public:

		vfilesession(ibis::file _id, ibis::string _vnode)
		{
			this->_id = _id;
			this->_vnode = _vnode;
		}

		ibis::file   id() const { return this->_id; }
		ibis::string node() const { return this->_vnode; }
	};
};

// End header guard
#endif
