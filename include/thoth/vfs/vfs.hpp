/*
* 	filename  : vfs.hpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _THOTH_VFS_VFS_HPP
#define _THOTH_VFS_VFS_HPP

// Thoth headers
#include <ibis/stdio.hpp>

// Ibis headers
#include <ibis/value.hpp>
#include <ibis/ref.hpp>
#include <ibis/string.hpp>

namespace thoth
{
	// Forward declaration
	struct vnode;
	struct vfilesession;

	ibis::status     vfs_init();
	ibis::status     vfs_populate();
	vnode*           vfs_get_root();
	void             vfs_display();

	vnode* vfs_get_path(ibis::string _path);

	ibis::file   vfs_file_open(ibis::string _path);
	ibis::status vfs_file_write(ibis::file _filesession, ibis::file_msg _msg);
	ibis::status vfs_file_close(ibis::file _filesession);
};

// End header guard
#endif
