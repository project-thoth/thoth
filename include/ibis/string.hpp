/*
* 	filename  : string.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_STRING_HPP
#define _IBIS_STRING_HPP

// GCC headers
#include <ibis/types.hpp>

namespace ibis
{
	struct string
	{
	protected:

		char* _buffer = nullptr;

	public:

		string();
		string(memint _size);
		string(const char* _str);
		string(const char* _str, memint _len);
		string(const string& _str);
		~string();

		void        set(const char* _str, memint _len);
		const char* data() const;
		memint      length() const;
		void        append(const string& _other);

		string  substr(memint _offset, smemint _len = -1);
		smemint find_first(char _c);

		string& operator=(const string& _other);
		string& operator+(const string& _other);
		string& operator+=(const string& _other);
		string& operator+(const char* _str);
		string& operator+=(const char* _str);
		char&   operator[](memint i) const;
		string& operator=(const char* _str);
		bool    operator==(const string& _other) const;
		bool    operator==(const char* _str) const;
	};

	string to_string(smemint _i, int _base = 10);
}

// End header guard
#endif
