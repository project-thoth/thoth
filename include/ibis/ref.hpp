/*
* 	filename  : ref.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_REF_HPP
#define _IBIS_REF_HPP

// Ibis headers
#include <ibis/types.hpp>

namespace ibis
{
	template <typename T>
	struct ref_counter
	{
		T* _obj;
		memint _count;

		ref_counter(T* _obj, memint _count = 1)
		{
			this->_obj = _obj;
			this->_count = _count;
		}

		void increment() { this->_count ++; }
		void decrement() { this->_count --; }

		void update()
		{
			if (this->_count <= 0)
			{
				delete this->_obj;
				delete this;
			}
		}
	};

	template <typename T>
	struct ref
	{
	public:

		ref_counter<T>* _counter = nullptr;

		ref()
		{
		}

		ref(T* _obj)
		{
			this->_counter = new ref_counter<T>(_obj, 1);
		}

		ref(const ref<T>& r)
		{
			this->_counter = r._counter;
			this->_counter->increment();
			this->_counter->update();
		}

		ref<T> clone() { return ref(*this); }

		~ref()
		{
			this->_counter->decrement();
			this->_counter->update();
		}

		T* val() { return this->_counter->_obj; }

		ref<T>& operator=(const ref<T>& _other)
		{
			this->_counter = _other._counter;
			this->_counter->increment();
			return *this;
		}

		ref<T>& operator=(ref<T>&& _other)
		{
			this->_counter = _other._counter;
			this->_counter->increment();
			return *this;
		}
	};
}

// End header guard
#endif
