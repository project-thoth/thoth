/*
* 	filename  : stdio.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_STDIO_HPP
#define _IBIS_STDIO_HPP

// Ibis headers
#include <ibis/string.hpp>
#include <ibis/value.hpp>

namespace ibis
{
	enum class file_msg_op
	{
		DEFAULT    = 1,
		CLEAR      = 2,
		FORECOLOUR = 3,
		BACKCOLOUR = 4,
	};

	struct file_msg
	{
		unsigned int _op;
		byte* _data;
		memint _size;

		file_msg(byte* _data, memint _size, unsigned int _op = (unsigned int)file_msg_op::DEFAULT)
		{
			this->_data = _data;
			this->_size = _size;
			this->_op = _op;
		}

		unsigned int op() const { return this->_op; }
		byte*      data() const { return this->_data; }
		memint     size() const { return this->_size; }
	};

	struct file
	{
	private:

		unsigned long _id;

	public:

		file(unsigned long _id = 0)
		{
			this->_id = _id;
		}

		unsigned long id() { return this->_id; }
		bool valid() { return this->_id != 0; }

		status write(file_msg _msg);
		status close();
	};

	extern file stdout;

	file open(ibis::string _path);

	status print(string _str);
	status print_line(string _str);
	status print_format(string _fmt, ...);






	const uint64 ID_INVALID = (unsigned)18446744073709551615;

	const uint32 CHANNEL_STD = 0;
	const uint32 CHANNEL_SIZE_1 = (1 << 0);
	const uint32 CHANNEL_SIZE_2 = (2 << 0);
	const uint32 CHANNEL_SIZE_3 = (3 << 0);
	const uint32 CHANNEL_SIZE_4 = (4 << 0);
	const uint32 CHANNEL_SIZE_5 = (5 << 0);
	const uint32 CHANNEL_SIZE_6 = (6 << 0);
	const uint32 CHANNEL_SIZE_7 = (7 << 0);
	const uint32 CHANNEL_SIZE_8 = (8 << 0);

	const uint8 FILE_MODE_NONE = 0;
	const uint8 FILE_MODE_READ = 1;
	const uint8 FILE_MODE_WRITE = 2;
	const uint8 FILE_MODE_READWRITE = 3;

	struct stream
	{
		uint64 _id = ID_INVALID;

		uint16 write(const ptr _data, uint16 _n, uint16 _channel = CHANNEL_STD, uint16 _size = CHANNEL_SIZE_1);
		uint16 read(ptr _buffer, uint16 _n, uint32 _channel = CHANNEL_STD);
	};

	stream file_open(string _path, uint8 _mode = FILE_MODE_READ);
}

// End header guard
#endif
