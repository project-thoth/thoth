/*
* 	filename  : vga.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Local headers
#include <thoth/x86-family/vga.hpp>
#include <thoth/x86-family/port.hpp>
#include <thoth/x86-family/earlyvga.hpp>

// Thoth headers
#include <thoth/vfs/vfs.hpp>
#include <thoth/vfs/vnode.hpp>
#include <thoth/early.hpp>
#include <thoth/ibis.hpp>

namespace thoth
{
	uint16* VGA_BUFFER = (uint16*)0xB8000;
	uint16  VGA_WIDTH = 80;
	uint16  VGA_HEIGHT = 25;

	bool vga_initiated = false;

	vga_colour vga_fg = vga_colour::LIGHT_CYAN;
	vga_colour vga_bg = vga_colour::BLACK;
	memint     vga_cursor_column = 0;
	memint     vga_cursor_row = 0;

	ibis::status vga_init(vga_inherit_mode _inherit_mode)
	{
		thoth::vnode* dev_node = nullptr;
		thoth::vnode* vga_node = nullptr;

		// Bind VGA driver to /dev/vga file hook
		dev_node = vfs_get_path("/dev");
		if (dev_node != nullptr)
			vga_node = dev_node->add_vnode("vga");

		if (dev_node == nullptr)
			early_print("vga driver : Failed to find vnode '/dev'\n");
		else if (vga_node == nullptr)
			early_print("vga driver : Failed to create vnode '/dev/vga'\n");
		else
		{
			bool bind_success = vga_node->bind_write_hook(vga_process_msg);

			if (bind_success)
				early_print("vga driver : Successfully bound to '/dev/vga' write hook\n");
			else
				early_print("vga driver : Failed to bind to '/dev/vga' write hook\n");
		}

		switch (_inherit_mode)
		{
		case vga_inherit_mode::NONE:
			{
				vga_cursor_column = 0;
				vga_cursor_row = 0;

				vga_fg = vga_colour::LIGHT_GREEN;
				vga_bg = vga_colour::BLACK;

				vga_clear(vga_make_colour(vga_fg, vga_bg));
			}
			break;

		case vga_inherit_mode::CURSOR:
			{
				vga_cursor_column = early_vga_cursor_column;
				vga_cursor_row = early_vga_cursor_row;

				vga_fg = vga_colour::LIGHT_GREEN;
				vga_bg = vga_colour::BLACK;

				//vga_clear(vga_make_colour(vga_fg, vga_bg));
			}
			break;

		case vga_inherit_mode::ALL:
			{
				vga_cursor_column = early_vga_cursor_column;
				vga_cursor_row = early_vga_cursor_row;

				vga_fg = early_vga_fg;
				vga_bg = early_vga_bg;

				//vga_clear(vga_make_colour(vga_fg, vga_bg));
			}
			break;

		default:
			break;
		}

		vga_place_cursor(vga_cursor_column, vga_cursor_row);

		// Finish initiation
		vga_initiated = true;

		return ibis::status(ibis::result::SUCCESS);
	}

	uint8 vga_make_colour(vga_colour _fg, vga_colour _bg)
	{
		return ((uint8)_bg << 4) | (uint8)_fg;
	}

	uint16 vga_make_entry(char _c, uint8 _colour)
	{
		return ((uint16)_colour << 8) | (uint16)_c;
	}

	void vga_place_entry(char _c, uint8 _colour, memint _i, memint _j)
	{
		const memint index = _j * VGA_WIDTH + _i;
		VGA_BUFFER[index] = vga_make_entry(_c, _colour);
	}

	void vga_place_cursor(memint _i, memint _j)
	{
		uint16 pos = _j * VGA_WIDTH + _i;

		// Cursor LOW port to VGA INDEX register
		port_out8(0x3D4, 0x0F);
		port_out8(0x3D5, (uint8)(pos & 0xFF));
		// Cursor HIGH port to VGA INDEX register
		port_out8(0x3D4, 0x0E);
		port_out8(0x3D5, (uint8)((pos >> 8) & 0xFF));
	}

	void vga_clear(uint8 colour)
	{
		for (int i = 0; i < VGA_WIDTH; i ++)
		{
			for (int j = 0; j < VGA_HEIGHT; j ++)
			{
				vga_place_entry('\0', colour, i, j);
			}
		}

		vga_cursor_column = 0;
		vga_cursor_row = 0;
	}

	void vga_print_char(char _c)
	{
		uint8 colour = vga_make_colour(vga_fg, vga_bg);
		bool printable = true;
		bool new_row = false;

		if (_c == '\n')
		{
			vga_cursor_row ++;
			vga_cursor_column = 0;
			new_row = true;
			printable = false;
		}

		if (printable) // Skip if not printable
		{
			vga_place_entry(_c, colour, vga_cursor_column, vga_cursor_row);
			vga_cursor_column ++;
		}

		// Overflow horizontally
		if (vga_cursor_column >= VGA_WIDTH)
		{
			vga_cursor_row ++;
			vga_cursor_column = 0;
			new_row = true;
		}

		// Overflow vertically
		if (vga_cursor_row >= VGA_HEIGHT)
			vga_cursor_row = 0;

		// Clear current row
		if (new_row)
		{
			for (int i = 0; i < VGA_WIDTH; i ++)
				vga_place_entry(' ', colour, i, vga_cursor_row);
		}

		vga_place_cursor(vga_cursor_column, vga_cursor_row);
	}

	void vga_print_string(const char* _str)
	{
		for (memint i = 0; _str[i] != '\0'; i ++)
		{
			vga_print_char(_str[i]);
		}
	}

	void vga_process_msg(const ibis::file_msg _msg, const vfilesession* _session)
	{
		switch (_msg.op())
		{
		case (unsigned int)ibis::file_msg_op::DEFAULT:
			{
				for (memint i = 0; i < _msg.size(); i ++)
					vga_print_char(((const char*)_msg.data())[i]);
			}
			break;

		case (unsigned int)ibis::file_msg_op::CLEAR:
			{
				if (_msg.size() == 0) // If it's 0 bytes, clear the screen with default values
				{
					vga_clear(vga_make_colour(vga_fg, vga_bg));
					break;
				}

				if (_msg.size() == 2) // If it's 2 bytes, extract values
				{
					byte fg = _msg.data()[0];
					byte bg = _msg.data()[1];
					vga_clear(vga_make_colour((vga_colour)fg, (vga_colour)bg));
				}
			}
			break;

		case (unsigned int)ibis::file_msg_op::FORECOLOUR:
			{
				if (_msg.size() == 1) // If it's 1 byte, extract values
				{
					byte col = _msg.data()[0];
					vga_fg = (vga_colour)col;
				}
			}
			break;

		case (unsigned int)ibis::file_msg_op::BACKCOLOUR:
			{
				if (_msg.size() == 1) // If it's 1 byte, extract values
				{
					byte col = _msg.data()[0];
					vga_bg = (vga_colour)col;
				}
			}
			break;

		default:
			break;
		}
	}
}
