/*
* 	filename  : interrupt.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/x86-family/interrupt.hpp>
#include <thoth/x86-family/port.hpp>
#include <thoth/early.hpp>
#include <thoth/kentry.hpp>

#include <ibis/stdio.hpp>

namespace thoth
{
	const memint idt_size = 256 + 0x20;

	struct idt_entry
	{
		unsigned short int offset_lowerbits;
		unsigned short int selector;
		unsigned char zero;
		unsigned char type_attr;
		unsigned short int offset_higherbits;
	};// __attribute__((packed));

	struct idt
	{
		idt_entry entries[idt_size];
	};// __attribute__((packed));

	idt g_idt;

	extern "C" uint32 idt_ptr[2];
	uint32 idt_ptr[2];

	extern "C" void kbd_irq_handler();
	asm volatile("kbd_irq_handler: \n cli \n call kbd_irq_handler_main \n sti \n iret");

	extern "C" void default_irq_handler();
	asm volatile("default_irq_handler: \n cli \n call default_irq_handler_main \n sti \n iret");

	void interrupt_init()
	{
		// Set default handler (with 32 offset)
		for (int i = -0x20; i < 256; i ++)
		{
			unsigned long default_irq_address = (unsigned long)default_irq_handler;
			g_idt.entries[0x20 + i].offset_lowerbits = default_irq_address & 0xFFFF;
			g_idt.entries[0x20 + i].selector = 0x10; // Kernel code segment offset
			g_idt.entries[0x20 + i].zero = 0;
			g_idt.entries[0x20 + i].type_attr = 0x8E; // Interrupt gate
			g_idt.entries[0x20 + i].offset_higherbits = (default_irq_address & 0xFFFF0000) >> 16;
		}

		// Set keyboard handler (with 32 offset)
		unsigned long kbd_irq_address = (unsigned long)kbd_irq_handler;
		g_idt.entries[0x21].offset_lowerbits = kbd_irq_address & 0xFFFF;
		g_idt.entries[0x21].selector = 0x10; // Kernel code segment offset
		g_idt.entries[0x21].zero = 0;
		g_idt.entries[0x21].type_attr = 0x8E; // Interrupt gate
		g_idt.entries[0x21].offset_higherbits = (kbd_irq_address & 0xFFFF0000) >> 16;

		// Init ICW1
		port_out8(0x20, 0x11);
		port_out8(0xA0, 0x11);

		// Init ICW2 (remap offset address)
		// x86 protected mode requires us to remap the PICs beyond 0x20 since first 32 are reserved
		port_out8(0x21, 0x20);
		port_out8(0xA1, 0x28);

		// Init ICW3 (cascading)
		port_out8(0x21, 0x00);
		port_out8(0xA1, 0x00);

		// Init ICW4 (environment information)
		port_out8(0x21, 0x01);
		port_out8(0xA1, 0x01);

		// Mask interrupts
		port_out8(0x21, 0xFF);
		port_out8(0xA1, 0xFF);

		// Create the IDT descriptor
		unsigned long idt_address = (unsigned long)g_idt.entries;
		ibis::print_format("IDT ADDRESS = %X\n", idt_address);
		ibis::print_format("IRQ ADDRESS = %X\n", (unsigned long)kbd_irq_handler);
		idt_ptr[0] = (sizeof(struct idt_entry) * idt_size) + ((idt_address & 0xFFFF) << 16);
		idt_ptr[1] = idt_address >> 16;

		//asm volatile("lidt (%0) \n sti" :: "a"(idt_ptr));
		asm volatile ("lidt ((idt_ptr)) \n sti");

		early_print("Initiated interrupts\n");
	}

	extern "C" void kbd_irq_handler_main()
	{
		byte status;
		char keycode;

		/* write EOI */
		port_out8(0x20, 0x20);
		//return;

		status = port_in8(0x60).data();//KEYBOARD_STATUS_PORT);
		/* Lowest bit of status will be set if buffer is not empty */
		if (status != 0) //& 0x01 || false)
		{
			keycode = port_in8(0x60).data();//KEYBOARD_DATA_PORT);

			if (keycode < 0)
				return;

			char c[] = "c";
			c[0] = "!!1234567890-=!!qwertyuiop[]!!asdfghjkl;'#!\\zxcvbnm,./!!! !FFFFFFFFFF!"[keycode];
			early_print(c);
		}
	}

	extern "C" void default_irq_handler_main()
	{
		early_print("Interrupt occured!\n");
	}
}
