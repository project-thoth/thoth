//
//	filename  : boot.s
//	component : thoth
//
//	This file is part of Thoth.
//
//	Thoth is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	Thoth is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	You should have received a copy of the GNU General Public License
//	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
//

// Multiboot constants
.set MB_ALIGN,    1 << 0
.set MB_MEMINFO,  1 << 1
.set MB_FLAGS,    MB_ALIGN | MB_MEMINFO
.set MB_MAGIC,    0x1BADB002
.set MB_CHECKSUM, -(MB_MAGIC + MB_FLAGS)

// The Multiboot-compliant header
.section .multiboot
	.global _multiboot
	_multiboot:
		.align 4
		.long MB_MAGIC
		.long MB_FLAGS
		.long MB_CHECKSUM

		.long _multiboot
		.long 0
		.long 0
		.long 0

// A temporary 16 KB bootstrap stack
.section .boot_stack, "aw", @nobits
	_boot_stack_bottom:
	.skip 0x4000 // 16 KB
	_boot_stack_top:

// Now for some actual code
.section .boot_text
	.global _boot_entry
	.type _boot_entry, @function
	_boot_entry:
		// Tell the stack pointer where the bootstrap stack is
		movl $_boot_stack_top, %esp

		// We now have a C-worthy (get it?) environment
		// Time to jump into kernel early C
		call kearly

		// C++ constructor code
		call _init

		// Main kernel code
		call kmain

		// C++ destructor code
		call _fini

		// If the kernel call returns, halt the system
		jmp _khalt

	.global _khalt
	_khalt:
		cli
		hlt // Halt the CPU
		jmp _khang // If halting fails, hang the CPU instead

	.global _khang
	_khang:
		jmp _khang // Should the CPU become un-halted, hang it instead

	// Set the size of the _boot_entry label to the current location minus its beginning position
	.size _boot_entry, . - _boot_entry
