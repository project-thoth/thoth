/*
* 	filename  : earlyvga.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/x86-family/earlyvga.hpp>
#include <thoth/x86-family/vga.hpp>

namespace thoth
{
	vga_colour early_vga_fg = vga_colour::LIGHT_CYAN;
	vga_colour early_vga_bg = vga_colour::BLACK;
	memint     early_vga_cursor_column = 0;
	memint     early_vga_cursor_row = 0;

	void early_clear_vga()
	{
		uint8 colour = vga_make_colour(early_vga_fg, early_vga_bg);

		vga_clear(colour);
	}

	void early_print_vga(const char* _str)
	{
		uint8 colour = vga_make_colour(early_vga_fg, early_vga_bg);

		for (memint i = 0; _str[i] != '\0'; i ++)
		{
			if (_str[i] == '\n')
			{
				early_vga_cursor_row ++;
				early_vga_cursor_column = 0;
				vga_place_cursor(early_vga_cursor_column, early_vga_cursor_row);
				continue;
			}

			vga_place_entry(_str[i], colour, early_vga_cursor_column, early_vga_cursor_row);

			early_vga_cursor_column ++;
			if (early_vga_cursor_column >= VGA_WIDTH)
			{
				early_vga_cursor_row ++;
				early_vga_cursor_column = 0;
			}

			vga_place_cursor(early_vga_cursor_column, early_vga_cursor_row);
		}
	}
};
