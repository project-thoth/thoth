/*
* 	filename  : string.cpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/string.hpp>
#include <ibis/memory.hpp>
#include <ibis/math.hpp>

#include <thoth/early.hpp>

namespace ibis
{
	const int STRING_NULL = 0;

	string::string()
	{
		this->_buffer = (char*)allocate(sizeof(char) * 1).data();
		this->_buffer[0] = STRING_NULL;
	}

	string::string(memint _size)
	{
		this->_buffer = (char*)allocate(sizeof(char) * (_size + 1)).data();
		this->_buffer[0] = STRING_NULL;
	}

	string::string(const char* _str)
	{
		memint len = 0;
		for (; _str[len] != '\0'; len ++) {}
		this->set(_str, len);
	}

	string::string(const char* _str, memint _len)
	{
		this->set(_str, _len);
	}

	string::string(const string& _str)
	{
		if (&_str == this)
			return;

		memint len = 0;
		for (; _str.data()[len] != STRING_NULL; len ++) {}

		this->set(_str.data(), len);
	}

	string::~string()
	{
		if (this->_buffer != nullptr)
			deallocate(this->_buffer);
	}

	const char* string::data() const
	{
		return this->_buffer;
	}

	size_t string::length() const
	{
		memint len = 0;
		for (; this->_buffer[len] != '\0'; len ++) {}
		return len;
	}

	void string::set(const char* _str, memint _len)
	{
		if (this->_buffer != nullptr)
			deallocate(this->_buffer);

		memint len = 0;
		for (; _str[len] != '\0'; len ++) {}

		this->_buffer = (char*)allocate(sizeof(char) * (min(_len, len) + 1)).data();

		memint i = 0;
		for (; i < _len; i ++) _buffer[i] = _str[i];
		this->_buffer[i] = STRING_NULL;
	}

	void string::append(const string& _other)
	{
		const string* _otherstr;
		if (&_other == this)
			_otherstr = new string(_other);
		else
			_otherstr = &_other;

		memint thislen  = this->length();
		memint otherlen = _otherstr->length();
		memint sumlen   = thislen + otherlen;

		this->_buffer = (char*)reallocate((byte*)this->_buffer, sizeof(char) * (sumlen + 1)).data();

		for (memint i = 0; i < otherlen; i ++)
			this->_buffer[thislen + i] = (*_otherstr)[i];

		this->_buffer[sumlen] = STRING_NULL;
	}

	string string::substr(memint _offset, smemint _len)
	{
		if (_len == -1)
			return string(this->data() + _offset);
		else
			return string(this->data() + _offset, _len);
	}

	smemint string::find_first(char _c)
	{
		for (memint i = 0; i < this->length(); i ++)
		{
			if ((*this)[i] == _c)
				return i;
		}

		return -1;
	}

	string& string::operator=(const string& _other)
	{
		if (&_other == this)
			return *this;

		this->set("", 0);
		this->append(_other);
		return *this;
	}

	string& string::operator+(const string& _other)
	{
		this->append(_other);
		return *this;
	}

	string& string::operator+=(const string& _other)
	{
		this->append(_other);
		return *this;
	}

	string& string::operator+(const char* _str)
	{
		this->append(string(_str));
		return *this;
	}

	string& string::operator+=(const char* _str)
	{
		this->append(string(_str));
		return *this;
	}

	char& string::operator[](size_t _i) const
	{
		return this->_buffer[_i];
	}

	string& string::operator=(const char* _str)
	{
		this->set("", 0);
		this->append(string(_str));
		return *this;
	}

	bool string::operator==(const string& _other) const
	{
		for (memint i = 0; this->_buffer[i] != STRING_NULL && _other.data()[i] != STRING_NULL; i ++)
		{
			if (this->_buffer[i] != _other.data()[i])
				return false;
		}

		return true;
	}

	bool string::operator==(const char* _str) const
	{
		return string(_str) == *this;
	}

	string to_string(smemint _value, int _base)
	{
		smemint v = 1;
		int i = 0;
		string str(sizeof(smemint) + 2);

		if (_base < 2 || _base > 32)
			return str;

		if (_value < 0)
		{
			str[i] = '-';
			i ++;
		}

		_value = abs(_value);

		while (v < (smemint)((memint)(-1) / 4) / _base)
			v *= _base;

		while (v > 0)
		{
			if (_value / v > 0)
			{
				if (((_value % (v * _base)) / v) % _base >= 10)
					str[i] = 'A' + (_value / v) % _base - 10;
				else
					str[i] = '0' + (_value / v) % _base;

				i ++;
			}
			v /= _base;
		}

		if (_value == 0)
		{
			str[i] = '0';
			i ++;
		}

		str[i] = '\0';
		return str;
	}
}
