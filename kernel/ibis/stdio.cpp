/*
* 	filename  : stdio.cpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/stdio.hpp>
#include <ibis/math.hpp>
#include <ibis/char.hpp>

// Thoth headers
#include <thoth/vfs/vfs.hpp>
#include <thoth/kerror.hpp>

namespace ibis
{
	file stdout;

	file open(ibis::string _path)
	{
		return thoth::vfs_file_open(_path);
	}

	status file::write(file_msg _msg)
	{
		thoth::kerror_push(__func__);
		status write_status = thoth::vfs_file_write(*this, _msg);
		thoth::kerror_pop();
		return write_status;
	}

	status file::close()
	{
		return thoth::vfs_file_close(*this);
	}

	status print(string _str)
	{
		return stdout.write(file_msg((byte*)_str.data(), _str.length() * sizeof(char)));
	}

	status print_line(string _str)
	{
		return print(_str + "\n");
	}

	status print_format(string _fmt, ...)
	{
		size_t len = 0;
		size_t format_len = _fmt.length();

		string final = "";

		__builtin_va_list args;
		__builtin_va_start(args, _fmt);

		size_t last = 0;
		size_t i;
		for (i = 0; i < format_len; i ++)
		{
			switch (_fmt[i])
			{
				case '%':
				{
					if (i >= format_len - 1) // Make sure we're not at the end of the string
						break;

					switch (_fmt[i + 1])
					{
						case 's': // We're writing a string
						{
							final += string((const char*)&(_fmt[last]), i - last); // Write the string so far
							// Write the string we found in the gap
							string str = string(__builtin_va_arg(args, char*));
							final += str; // Write the inserted string
							len += str.length();
							last = i + 2; // The string format sequence was 2 characters long
							break;
						}
						case 'd': // We're writing a signed int
						case 'i':
						case 'x': // We're writing a hexadecimal int
						case 'X':
						case 'u': // We're writing an unsigned int
						case 'o': // We're writing an octal int
						{
							final += string((const char*)&(_fmt[last]), i - last); // Write the string so far

							// Write the string we found in the gap
							string str;

							int v = __builtin_va_arg(args, int);

							if (_fmt[i + 1] == 'u') // Is it unsigned?
								v = abs(v);

							int base = (_fmt[i + 1] == 'x' || _fmt[i + 1] == 'X') ? 16 : ((_fmt[i + 1] == 'o') ? 8 : 10); // Which base are we using?
							str = to_string(v, base); // Convert it to a string

							if (base == 16 && _fmt[i + 1] == 'x') // Convert it to lowercase if we have to
							{
								for (int j = 0; str[j] != '\0'; j ++)
									str[j] = lowercase(str[j]);
							}

							final += str; // Write the inserted integer
							len += str.length();
							last = i + 2; // The integer format sequence was 2 characters long
							break;
						}
						case 'c':
						{
							final += string((const char*)&(_fmt[last]), i - last); // Write the string so far

							// Write the character we found in the gap
							char character = (char)__builtin_va_arg(args, int);
							final += string((const char*)&character, 1); // Write the inserted character
							len ++;
							last = i + 2; // The character format sequence was 2 characters long
							break;
						}
						default:
							break;
					}
				}
				default:
					break;
			}

			len ++;
		}

		final += string((const char*)&(_fmt[last]), i - last); // Write any trailing string

		print(final);

		__builtin_va_end(args);

		return status(result::SUCCESS);
	}

	value<file> file_open(const char* _path);
	status      file_write(file _file);
}
