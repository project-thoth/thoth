/*
* 	filename  : memory.cpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/memory.hpp>

// Thoth headers
#include <thoth/mempool.hpp>

namespace ibis
{
	value<ptr> allocate(memint _n)
	{
		return thoth::mempool_alloc(_n);
	}

	value<ptr> reallocate(ptr _ptr, memint _n)
	{
		return thoth::mempool_realloc(_ptr, _n);
	}

	status deallocate(ptr _ptr)
	{
		return thoth::mempool_dealloc(_ptr);
	}
}

ptr operator new(memint _n)
{
	return (ptr)ibis::allocate(_n).data();
}

ptr operator new[](memint _n)
{
	return (ptr)ibis::allocate(_n).data();
}

void operator delete(ptr _ptr)
{
	ibis::deallocate((byte*)_ptr);
}

void operator delete[](ptr _ptr)
{
	ibis::deallocate((byte*)_ptr);
}
