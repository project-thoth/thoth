/*
* 	filename  : vnode.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/vfs/vnode.hpp>
#include <thoth/kerror.hpp>

#include <thoth/early.hpp>

// Ibis headers
#include <ibis/stdio.hpp>

namespace thoth
{
	vnode::vnode(ibis::string _name, vnode_flag_data _flags)
	{
		kerror_push(__func__);

		if (_name.length() > 0)
			this->init(_name, _flags);

		kerror_pop();
	}

	void vnode::init(ibis::string _name, vnode_flag_data _flags)
	{
		kerror_push(__func__);

		this->_name = _name;
		this->_flags.flag(_flags, VNODE_FLAG_MASK_ALL);

		kerror_pop();
	}

	vnode* vnode::get_vnode(ibis::string _name)
	{
		for (memint i = 0; i < this->_children.length(); i ++)
		{
			if (this->_children[i]->name() == _name)
				return this->_children[i];
		}

		return nullptr;
	}

	vnode* vnode::get_path(ibis::string _path)
	{
		smemint delim_pos = _path.find_first('/');

		if (delim_pos == -1)
		{
			//early_print((ibis::string("HERE2! ") + _path.substr(0, delim_pos) + "!\n").data()); while (true) {}
			return this->get_vnode(_path);
		}
		else
		{
			//early_print((ibis::string("HERE! ") + _path.substr(0, delim_pos) + "!\n").data());
			vnode* child = this->get_vnode(_path.substr(0, delim_pos));

			if (child == nullptr)
				return nullptr;

			return child->get_path(_path.substr(delim_pos + 1));
		}
	}

	vnode* vnode::add_vnode(ibis::string _name, vnode_flag_data _flags)
	{
		kerror_push(__func__);

		vnode* new_node = new vnode(_name, _flags);
		this->_children.push(new_node);

		kerror_pop();
		return new_node;
	}

	ibis::string vnode::name()
	{
		return this->_name;
	}

	ibis::vector<vnode*>& vnode::children()
	{
		return this->_children;
	}

	void vnode::write(const ibis::file_msg _msg, const vfilesession* _session)
	{
		if (this->func_write != nullptr)
			this->func_write(_msg, _session);
	}

	bool vnode::bind_write_hook(void (*func_write)(const ibis::file_msg, const vfilesession* _msg))
	{
		if (func_write != nullptr)
		{
			this->func_write = func_write;
			return true;
		}
		else
			return false;
	}
};
