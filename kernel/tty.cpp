/*
* 	filename  : tty.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/tty.hpp>
#include <thoth/vfs/vfs.hpp>
#include <thoth/vfs/vnode.hpp>
#include <thoth/console.hpp>

#include <thoth/early.hpp>

// Ibis headers
#include <ibis/char.hpp>

namespace thoth
{
	tty::tty(unsigned long _id, ibis::string _path)
	{
		this->_id = _id;
		this->_path = _path;

		this->init();
	}

	ibis::status tty::init()
	{
		thoth::vnode* console_node = nullptr;
		thoth::vnode* tty_node = nullptr;

		// Bind tty to /dev/console/ttyX file hook
		console_node = vfs_get_path("/dev/console");
		if (console_node != nullptr)
			tty_node = console_node->add_vnode(ibis::string("tty") + ibis::to_string(this->id()));

		if (console_node == nullptr)
			early_print("console driver : Failed to find vnode '/dev/console'\n");
		else if (tty_node == nullptr)
			early_print((ibis::string("console driver : Failed to create vnode '/dev/console/tty") + ibis::to_string(this->id()) + "'\n").data());
		else
		{
			bool bind_success = tty_node->bind_write_hook(console_process_tty_msg);

			if (bind_success)
				early_print((ibis::string("tty : Successfully bound to '/dev/console/tty") + ibis::to_string(this->id()) + "'\n").data());
			else
				early_print((ibis::string("tty : Failed to bound to '/dev/console/tty") + ibis::to_string(this->id()) + "'\n").data());
		}

		return ibis::status(ibis::result::SUCCESS);
	}

	void tty::process_msg(const ibis::file_msg _msg)
	{
		if (_msg.op() != (unsigned int)ibis::file_msg_op::DEFAULT) // If it's a non-standard message, redirect
			console_stdout.write(_msg);

		char*  data = (char*)_msg.data();
		memint n = _msg.size();

		for (memint i = 0; i < n; i ++)
		{
			int16 escaped = 0;

			if (data[i] == '$' && i + 1 < n)
			{
				if (escaped == 0 && data[i + 1] == 'F' && i + 2 < n)
				{
					if (ibis::is_hex(data[i + 2]))
					{
						escaped = 1;

						byte col[] = {ibis::to_hex(data[i + 2])};
						console_stdout.write(ibis::file_msg((byte*)col, 1, (unsigned int)ibis::file_msg_op::FORECOLOUR));

						i += 2;
					}
				}

				if (escaped == 0 && data[i + 1] == 'B' && i + 2 < n)
				{
					if (ibis::is_hex(data[i + 2]))
					{
						escaped = 1;

						byte col[] = {ibis::to_hex(data[i + 2])};
						console_stdout.write(ibis::file_msg((byte*)col, 1, (unsigned int)ibis::file_msg_op::BACKCOLOUR));

						i += 2;
					}
				}
			}

			if (escaped != 1)
			{
				byte c[] = {(byte)data[i]};
				console_stdout.write(ibis::file_msg((byte*)c, 1));
			}
		}
	}
};
