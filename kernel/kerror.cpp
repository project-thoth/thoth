/*
* 	filename  : kerror.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/kerror.hpp>
#include <thoth/early.hpp>

namespace thoth
{
	static char*        error_stack[64] = { nullptr };
	static unsigned int error_index = 0;

	void kerror(const char* _msg)
	{
		for (unsigned int i = 0; ; i ++)
		{
			for (unsigned int j = 0; j < i; j ++) early_print(" ");

			if (kerror_get(i) != nullptr)
			{
				early_print("in @function '");
				early_print(kerror_get(i));
				early_print("':\n");
			}
			else
			{
				early_print("error: ");
				early_print(_msg);
				early_print("\n");
				break;
			}
		}
	}

	void kerror_push(const char* _msg)
	{
		if (error_index >= 64)
			return;

		error_stack[error_index] = (char*)_msg;
		error_index ++;
	}

	void kerror_pop()
	{
		if (error_index <= 0)
			return;

		error_index --;
		error_stack[error_index] = nullptr;
	}

	const char* kerror_get(unsigned int _i)
	{
		if (_i >= error_index)
			return nullptr;

		return error_stack[_i];
	}
};
