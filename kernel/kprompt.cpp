/*
* 	filename  : kprompt.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Thoth headers
#include <thoth/kprompt.hpp>
#include <thoth/vfs/vfs.hpp>

// Ibis headers
#include <ibis/stdio.hpp>

namespace thoth
{
	static const char GPL3_LICENCE_SHORT[] = "$B8$FB\n\n Thoth$FF  $FCCopyright (C) 2016$FF  Joshua Barretto\n\n" \
	" This program comes with ABSOLUTELY NO WARRANTY. This is free software, and\n you are " \
	"welcome to redistribute it under certain conditions.\n\n You should have received a copy" \
	" of the GNU General Public License along with\n this program. If not, see <http://www.gn" \
	"u.org/licenses/>.\n$B0\n\n";

	void kprompt()
	{
		// Clear the screen
		ibis::stdout.write(ibis::file_msg(nullptr, 0, (unsigned int)ibis::file_msg_op::CLEAR));

		ibis::print_format("$FFWelcome to $F3Thoth$FF $FC%s$FF on $FB%s$FF\n", THOTH_VERSION, THOTH_ARCH);
		ibis::print_format("%s", GPL3_LICENCE_SHORT);

		//vfs_display();

		// Display the kernel prompt
		ibis::print("$FF[$F3joshua$FF@$F2thoth$FF]$ ");
	}
};
