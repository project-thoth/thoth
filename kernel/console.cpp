/*
* 	filename  : console.cpp
* 	component : thoth
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Ibis headers
#include <ibis/generic/vector.hpp>

// Thoth headers
#include <thoth/console.hpp>
#include <thoth/vfs/vfs.hpp>
#include <thoth/vfs/vnode.hpp>
#include <thoth/early.hpp>

#if defined(THOTH_ARCH_i686) || defined(THOTH_ARCH_x86_64)
	#include <thoth/x86-family/vga.hpp>
#endif

namespace thoth
{
	ibis::vector<tty>* console_ttys = nullptr;
	unsigned long console_active_tty = 0;
	unsigned long console_tty_id_count = 1;

	vnode* console_root_node = nullptr;

	ibis::file console_stdin(0);
	ibis::file console_stdout(0);

	ibis::status console_init(ibis::string _in, ibis::string _out)
	{
		console_ttys = new ibis::vector<tty>();

		thoth::vnode* dev_node = nullptr;
		thoth::vnode* console_node = nullptr;

		// Bind VGA driver to /dev/vga file hook
		dev_node = vfs_get_path("/dev");
		if (dev_node != nullptr)
		{
			console_node = dev_node->add_vnode("console", VNODE_FLAG_TYPE_DIR);
			console_root_node = console_node;
		}

		if (dev_node == nullptr)
			early_print("console driver : Failed to find vnode '/dev'\n");
		else if (console_node == nullptr)
			early_print("console driver : Failed to create vnode '/dev/console'\n");
		else
		{
			bool bind_success = console_node->bind_write_hook(console_process_msg);

			if (bind_success)
				early_print("console driver : Successfully bound to '/dev/console' write hook\n");
			else
				early_print("console driver : Failed to bind to '/dev/console' write hook\n");
		}

		// Set the out file node
		ibis::file out_file = ibis::open(_out);
		if (out_file.valid())
			console_stdout = out_file;
		else
			early_print((ibis::string("Console Driver : Failed to open stdout '") + _out + "'\n").data());

		return ibis::status(ibis::result::SUCCESS);
	}

	static unsigned long console_gen_tty_id()
	{
		console_tty_id_count ++;
		return console_tty_id_count - 1;
	}

	ibis::value<unsigned long> console_gen_tty()
	{
		if (console_ttys == nullptr)
			return ibis::value<unsigned long>(0, ibis::result::FAILURE);

		unsigned long new_id = console_gen_tty_id();
		tty new_tty(new_id, ibis::string("/dev/console/tty") + ibis::to_string(new_id));

		console_ttys->push(new_tty);

		return ibis::value<unsigned long>(new_id, ibis::result::SUCCESS);
	}

	ibis::value<ibis::string> console_get_tty(unsigned long _i)
	{
		if (console_ttys == nullptr)
			ibis::value<ibis::string>("", ibis::result::FAILURE);

		for (memint i = 0; i < console_ttys->length(); i ++)
		{
			if ((*console_ttys)[i].id() == _i)
				return ibis::value<ibis::string>((*console_ttys)[i].path(), ibis::result::SUCCESS);
		}

		return ibis::value<ibis::string>("", ibis::result::FAILURE);
	}

	ibis::status console_switch_tty(unsigned long _i)
	{
		console_active_tty = _i;

		return ibis::status(ibis::result::SUCCESS);
	}

	void console_process_msg(const ibis::file_msg _msg, const vfilesession* _session)
	{
		early_print("Console received message\n");
	}

	void console_process_tty_msg(const ibis::file_msg _msg, const vfilesession* _session)
	{
		if (console_ttys == nullptr)
			return;

		for (memint i = 0; i < console_ttys->length(); i ++)
		{
			if ((*console_ttys)[i].path() == _session->node())
			{
				(*console_ttys)[i].process_msg(_msg);
			}
		}
	}
};
